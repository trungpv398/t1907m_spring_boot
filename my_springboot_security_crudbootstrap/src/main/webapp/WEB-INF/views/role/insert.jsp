
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<jsp:include page="../layout/header.jsp"></jsp:include>
<br>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h1> Registration </h1>
            <form:form action="saveRoleNew" method="post" modelAttribute="role">
                <div class="form-group">
                    <label class="control_label" for="name">First Name</label>
                    <form:input id="name" class="form-control" path="name" autofocus="autofocus"/>
                    <form:errors path="name" cssClass="colorRed"/>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success">Save</button>

                </div>
            </form:form>

        </div>
    </div>
</div>
<jsp:include page="../layout/footer.jsp"></jsp:include>
