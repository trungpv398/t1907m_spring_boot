package trungt1907m.my_springboot_security_crudbootstrap.model;


import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @NotEmpty(message = "Tên không được để trống")
    @Column(name = "pro_name")
    private String pro_name;
    @NotEmpty(message = "Mô tả không để trống")
    @Column(name = "pro_desc")
    private String  pro_desc;

    @Column(name = "image")
    private String image;

    public Product() {
    }


    public Product(String pro_name, String pro_desc) {
        this.pro_name = pro_name;
        this.pro_desc = pro_desc;
    }

    public Product(@NotEmpty(message = "Tên không được để trống") String pro_name, @NotEmpty(message = "Mô tả không để trống") String pro_desc, String image) {
        this.pro_name = pro_name;
        this.pro_desc = pro_desc;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public String getPro_desc() {
        return pro_desc;
    }

    public void setPro_desc(String pro_desc) {
        this.pro_desc = pro_desc;
    }

    public String getImage() {
        if (image == null )return null;
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
