package trungt1907m.my_springboot_security_crudbootstrap.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import trungt1907m.my_springboot_security_crudbootstrap.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product,Integer> {
}
