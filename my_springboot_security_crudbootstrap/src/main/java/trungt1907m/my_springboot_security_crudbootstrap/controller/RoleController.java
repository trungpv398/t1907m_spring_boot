package trungt1907m.my_springboot_security_crudbootstrap.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import trungt1907m.my_springboot_security_crudbootstrap.model.Role;
import trungt1907m.my_springboot_security_crudbootstrap.service.RoleService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(path = "/role")
public class RoleController {
    @Autowired
    private RoleService roleService;

    @RequestMapping(path = {"/",""})
    public String getListRole(Model model)
    {
        List<Role> listRole = roleService.getAllRole();
        model.addAttribute("list",listRole);
        return "role/listRole";
    }

    @RequestMapping(path = "/addNew")
    public String insertRole(Model model)
    {
        Role role = new Role();
        model.addAttribute("role",role);
        return "role/insert";
    }

    @RequestMapping(value = "/saveRoleNew",method = RequestMethod.POST)
    public String saveRole(@Valid @ModelAttribute("role")Role role, Model model, BindingResult bindingResult)
    {
        if (bindingResult.hasErrors())
        {
            model.addAttribute("role",role);
            return "role/insert";
        }else {
            boolean b = roleService.inserRole(role);
            if (b)
            {
                model.addAttribute("success","Data entered successfully");
                return "redirect:/role";
            }else {
                model.addAttribute("error","Input data failed");
                return "redirect:/role";
            }
        }


    }

}
