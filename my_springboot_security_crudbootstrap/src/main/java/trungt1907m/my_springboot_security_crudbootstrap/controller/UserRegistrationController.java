package trungt1907m.my_springboot_security_crudbootstrap.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import trungt1907m.my_springboot_security_crudbootstrap.model.User;
import trungt1907m.my_springboot_security_crudbootstrap.service.UserService;
import trungt1907m.my_springboot_security_crudbootstrap.validator.UserValidator;


import javax.validation.Valid;

@Controller
@RequestMapping("/registration")
public class UserRegistrationController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserValidator userValidator;
    @RequestMapping
    public String showRegistrationForm(Model model)
    {
        User userRegistrationDto = new User();
        model.addAttribute("user",userRegistrationDto);
        return "registration";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String registerUserAccount(@ModelAttribute("user") User userRegistrationDto, BindingResult bindingResult, Model model)
    {
        userValidator.validate(userRegistrationDto,bindingResult);
        if(bindingResult.hasErrors())
        {
            //model.addAttribute("user",userRegistrationDto);
            return "registration";
        }
        else
        {
            userService.save(userRegistrationDto);
            return "redirect:/registration?success";
        }
    }
}
