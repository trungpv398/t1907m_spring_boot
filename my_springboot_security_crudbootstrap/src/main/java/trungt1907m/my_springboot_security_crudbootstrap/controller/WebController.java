package trungt1907m.my_springboot_security_crudbootstrap.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import trungt1907m.my_springboot_security_crudbootstrap.model.User;
import trungt1907m.my_springboot_security_crudbootstrap.service.UserService;

import java.util.List;

@Controller
public class WebController {
    @Autowired
    private UserService userService;
    @RequestMapping(path = "/login")
    public String login()
    {
        return "login";
    }


    @RequestMapping(path = "/")
    public String home(Model model)
    {
        List<User> list = userService.getAllUser();
        model.addAttribute("list",list);
        return "index";
    }

    @RequestMapping(path = "/hom")
    public String home2(){
        return "home";
    }
}
