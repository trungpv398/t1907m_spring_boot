package trungt1907m.my_springboot_security_crudbootstrap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import trungt1907m.my_springboot_security_crudbootstrap.controller.ProductController;

import java.io.File;

@SpringBootApplication
public class MySpringbootSecurityCrudbootstrapApplication {

    public static void main(String[] args) {
        new File(ProductController.uploadingDir).mkdirs();
        SpringApplication.run(MySpringbootSecurityCrudbootstrapApplication.class, args);
    }

}
