package trungt1907m.my_springboot_security_crudbootstrap.validator;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import trungt1907m.my_springboot_security_crudbootstrap.model.User;
import trungt1907m.my_springboot_security_crudbootstrap.service.UserService;

@Component
public class UserValidator implements Validator {
    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        User user = (User) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"username","NotEmpty");
        if(user.getUsername().length()<6||user.getUsername().length()>32)
        {
            errors.rejectValue("username","Size.userRegistrationDto.username");

        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"email","NotEmpty");
        if(user.getEmail().length()<6||user.getEmail().length()>32)
        {
            errors.rejectValue("email","Size.userRegistrationDto.email");

        }
        if(userService.findByUsername(user.getEmail())!=null)
        {
            errors.rejectValue("email","Duplicate.userRegistrationDto.email");
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"password","NotEmpty");
        if (user.getPassword().length()<8||user.getPassword().length()>32)
        {
            errors.rejectValue("password","Size.userRegistrationDto.password");
        }
//        if(!user.getPasswordConfirm().equals(user.getPassword()))
//        {
//            errors.rejectValue("passwordConfirm","Diff.userForm.passwordConfirm");
//        }
    }
}
