package trungt1907m.my_springboot_security_crudbootstrap.service;

import trungt1907m.my_springboot_security_crudbootstrap.model.Role;

import java.util.List;
import java.util.Optional;

public interface RoleService {
    public List<Role> getAllRole();
    public boolean inserRole(Role role);
    public Optional<Role> getRoleById(long id);
    public boolean update(Role role);


}
