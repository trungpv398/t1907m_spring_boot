package trungt1907m.my_springboot_security_crudbootstrap.service;

import trungt1907m.my_springboot_security_crudbootstrap.model.Product;

import java.util.List;

public interface ProductService {
    public Product inserPro(Product p);
    public List<Product> getAllProduct();
    public Product getProById(int id);
    public boolean updatePro(Product p);
    public boolean deletePro(int id);
}
