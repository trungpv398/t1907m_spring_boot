package trungt1907m.my_springboot_security_crudbootstrap.service;

public interface SecurityService {
    String findLoggedInUsername();
    void autoLogin(String username,String password);
}
