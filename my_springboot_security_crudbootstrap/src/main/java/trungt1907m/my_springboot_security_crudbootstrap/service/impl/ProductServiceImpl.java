package trungt1907m.my_springboot_security_crudbootstrap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import trungt1907m.my_springboot_security_crudbootstrap.model.Product;
import trungt1907m.my_springboot_security_crudbootstrap.repository.ProductRepository;
import trungt1907m.my_springboot_security_crudbootstrap.service.ProductService;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product inserPro(Product p) {
        productRepository.save(p);
        return p;
    }

    @Override
    public List<Product> getAllProduct() {
        List<Product> list = productRepository.findAll();
        return list;
    }

    @Override
    public Product getProById(int id) {
        Product product = productRepository.findById(id).get();
        return product;
    }

    @Override
    public boolean updatePro(Product p) {
        Product product = productRepository.save(p);
        return true;
    }

    @Override
    public boolean deletePro(int id) {
        try{
            productRepository.deleteById(id);
            return true;
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return true;
    }
}
