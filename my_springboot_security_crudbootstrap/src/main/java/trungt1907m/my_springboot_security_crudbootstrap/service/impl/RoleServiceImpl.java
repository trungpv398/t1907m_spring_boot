package trungt1907m.my_springboot_security_crudbootstrap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import trungt1907m.my_springboot_security_crudbootstrap.model.Role;
import trungt1907m.my_springboot_security_crudbootstrap.repository.RoleRepository;
import trungt1907m.my_springboot_security_crudbootstrap.service.RoleService;

import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleRepository roleRepository;
    @Override
    public List<Role> getAllRole() {
        List<Role> listRole = roleRepository.findAll();
        return listRole;
    }

    @Override
    public boolean inserRole(Role role) {

           roleRepository.save(role);
            return true;


    }

    @Override
    public Optional<Role> getRoleById(long id) {
        Optional<Role> role = roleRepository.findById(id);
        return role;
    }

    @Override
    public boolean update(Role role) {
        long id = role.getId();
          try{
              Role roleUpdate = roleRepository.findById(id).get();
              roleUpdate.setName(role.getName());
              roleRepository.save(roleUpdate);
              return true;
          }catch (Exception  e)
          {
              e.printStackTrace();
          }
            return false;
    }
}
