package trungt1907m.my_springboot_security_crudbootstrap.service;


import org.springframework.security.core.userdetails.UserDetailsService;
import trungt1907m.my_springboot_security_crudbootstrap.model.User;

import java.util.List;

public interface UserService {
    User save(User user);
    User findByUsername(String username);
    public List<User> getAllUser();
}
